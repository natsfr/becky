EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:xc6slx-ft256
LIBS:Silabs_transceivers
LIBS:passives
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 5
Title ""
Date "23 feb 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 600  600  1200 900 
U 54E6F930
F0 "POWER" 50
F1 "power.sch" 50
$EndSheet
$Sheet
S 3200 2250 1600 2750
U 54E6F934
F0 "RADIO_SI446X" 50
F1 "si446x.sch" 50
F2 "RADIO_CS" I L 3200 4650 60 
F3 "RADIO_MOSI" I L 3200 4750 60 
F4 "RADIO_MISO" O L 3200 4850 60 
F5 "RADIO_CLK" I L 3200 4550 60 
F6 "RADIO_IRQ" O L 3200 4950 60 
F7 "RADIO_GPIO0" B R 4800 2350 60 
F8 "RADIO_GPIO1" B R 4800 2450 60 
F9 "RADIO_SDN" I L 3200 4450 60 
F10 "RADIO_GPIO3" B R 4800 2650 60 
F11 "RADIO_GPIO2" B R 4800 2550 60 
$EndSheet
$Sheet
S 5950 2250 2050 4350
U 54E6F937
F0 "FPGA" 50
F1 "fpga.sch" 50
$EndSheet
$Sheet
S 850  4050 1450 2550
U 54E93441
F0 "ARM" 50
F1 "ARM.sch" 50
$EndSheet
$EndSCHEMATC
