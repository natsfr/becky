EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:xc6slx-ft256
LIBS:Silabs_transceivers
LIBS:passives
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 5
Title ""
Date "23 feb 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L XC6SLX-FT256 U401
U 1 1 54E933AC
P 9800 2050
F 0 "U401" H 10200 3200 60  0000 C CNN
F 1 "XC6SLX-FT256" H 9850 950 60  0000 C CNN
F 2 "FTG256" H 10200 2100 60  0001 C CNN
F 3 "~" H 10200 2100 60  0000 C CNN
	1    9800 2050
	1    0    0    -1  
$EndComp
$Comp
L XC6SLX-FT256 U401
U 6 1 54EAED1F
P 9800 5300
F 0 "U401" H 10250 5750 60  0000 C CNN
F 1 "XC6SLX-FT256" H 9800 4800 60  0000 C CNN
F 2 "FTG256" H 10200 5350 60  0001 C CNN
F 3 "~" H 10200 5350 60  0000 C CNN
	6    9800 5300
	1    0    0    -1  
$EndComp
$Comp
L XC6SLX-FT256 U401
U 5 1 54EAED3A
P 7750 5650
F 0 "U401" H 8400 6450 60  0000 C CNN
F 1 "XC6SLX-FT256" H 7750 4800 60  0000 C CNN
F 2 "FTG256" H 8150 5700 60  0001 C CNN
F 3 "~" H 8150 5700 60  0000 C CNN
	5    7750 5650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 54EB3F43
P 8350 6450
F 0 "#PWR?" H 8350 6450 30  0001 C CNN
F 1 "GND" H 8350 6380 30  0001 C CNN
F 2 "" H 8350 6450 60  0000 C CNN
F 3 "" H 8350 6450 60  0000 C CNN
	1    8350 6450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8150 5050 8350 5050
Wire Wire Line
	8350 5050 8350 6450
Wire Wire Line
	8150 6300 8350 6300
Connection ~ 8350 6300
Wire Wire Line
	8150 6250 8350 6250
Connection ~ 8350 6250
Wire Wire Line
	8150 6200 8350 6200
Connection ~ 8350 6200
Wire Wire Line
	8150 6150 8350 6150
Connection ~ 8350 6150
Wire Wire Line
	8150 6100 8350 6100
Connection ~ 8350 6100
Wire Wire Line
	8150 6050 8350 6050
Connection ~ 8350 6050
Wire Wire Line
	8150 6000 8350 6000
Connection ~ 8350 6000
Wire Wire Line
	8150 5950 8350 5950
Connection ~ 8350 5950
Wire Wire Line
	8150 5900 8350 5900
Connection ~ 8350 5900
Wire Wire Line
	8150 5850 8350 5850
Connection ~ 8350 5850
Wire Wire Line
	8150 5800 8350 5800
Connection ~ 8350 5800
Wire Wire Line
	8150 5750 8350 5750
Connection ~ 8350 5750
Wire Wire Line
	8150 5700 8350 5700
Connection ~ 8350 5700
Wire Wire Line
	8150 5650 8350 5650
Connection ~ 8350 5650
Wire Wire Line
	8150 5600 8350 5600
Connection ~ 8350 5600
Wire Wire Line
	8150 5550 8350 5550
Connection ~ 8350 5550
Wire Wire Line
	8150 5500 8350 5500
Connection ~ 8350 5500
Wire Wire Line
	8150 5450 8350 5450
Connection ~ 8350 5450
Wire Wire Line
	8150 5400 8350 5400
Connection ~ 8350 5400
Wire Wire Line
	8150 5350 8350 5350
Connection ~ 8350 5350
Wire Wire Line
	8150 5300 8350 5300
Connection ~ 8350 5300
Wire Wire Line
	8150 5250 8350 5250
Connection ~ 8350 5250
Wire Wire Line
	8150 5200 8350 5200
Connection ~ 8350 5200
Wire Wire Line
	8150 5150 8350 5150
Connection ~ 8350 5150
Wire Wire Line
	8150 5100 8350 5100
Connection ~ 8350 5100
$EndSCHEMATC
